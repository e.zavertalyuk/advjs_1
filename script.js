"use strict"


//HW 5

// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.




const urlUsers = "https://ajax.test-danit.com/api/json/users"
const urlPosts = "https://ajax.test-danit.com/api/json/posts"

class Request {
   get(url) {
      return fetch(url).then((response) => response.json())
         .catch((error) => console.log(error))
   }
   deletePost(postId) {
      return fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
         method: 'DELETE',
      }).catch((error) => console.log(error))
   }
}

class Card {
   render(userObj, postObj) {
      try {
         const cardContainer = document.createElement('div');
         postObj.forEach(({ id, userId, title, body }) => {

            const user = userObj.find(({ id }) => id === userId);

            const { name, surname, email } = user;

            const cardDiv = document.createElement('div');
            cardDiv.setAttribute('class', 'wrapper')
            const cardTitle = document.createElement('p');
            cardTitle.style.textTransform = 'uppercase'
            const cardText = document.createElement('p');
            const cardName = document.createElement('p');
            const cardSurname = document.createElement('p');
            const cardEmail = document.createElement('p');
            const deletePostBtn = document.createElement('button');
            deletePostBtn.setAttribute('id', `${id}`)

            cardTitle.textContent = title;
            cardText.textContent = body;
            cardName.textContent = name;
            cardSurname.textContent = surname;
            cardEmail.textContent = email;
            cardEmail.style.color = 'blue';
            deletePostBtn.textContent = 'Delete Post'

            cardDiv.append(cardTitle, cardText, cardName, cardSurname, cardEmail, deletePostBtn);
            cardContainer.appendChild(cardDiv);
         });
         return cardContainer;
      } catch (err) {
         alert(err)
      }
   }
}

const usersRequest = new Request()
const postsRequest = new Request();
const card = new Card();

Promise.all([usersRequest.get(urlUsers), postsRequest.get(urlPosts)])
   .then(([usersData, postsData]) => {
      const cardContainer = card.render(usersData, postsData);
      document.body.appendChild(cardContainer);
      cardContainer.addEventListener('click', deletePostHandler);
   })
   .catch((error) => {
      console.error(error);
   });

const deletePostHandler = (event) => {
   const postId = event.target.id;
   const delRequest = new Request()
   delRequest.deletePost(postId)
      .then((response) => {
         if (response.status === 200) {
            const cardDiv = event.target.closest('.wrapper');
            cardDiv.remove();
         }
      })
      .catch((error) => {
         console.error(error);
      });
};



//HW 4

// AJAX  Asynchronous Javascript and XML. Позволяет подргужать данные с сервера без перезагрузки страницы и лучшего взаимодействия с пользоватлеем через запросы на сервер
// const requestUrl = 'https://ajax.test-danit.com/api/swapi/films';
// class Films {
//    constructor(url) {
//       this.url = url;
//    }

//    getFilms(url) {
//       return fetch(url)
//          .then(response => response.json())
//          .catch((error) => {
//             alert(error);
//          })
//    }

//    renderName(filmList) {
//       try {
//          const list = document.createElement('ul');
//          filmList.sort((a, b) => {
//             return a.episodeId - b.episodeId
//          })

//          const filmName = filmList.map(({ name, episodeId, openingCrawl, characters }) => {
//             let liName = document.createElement('li');
//             const loader = document.createElement('div');
//             loader.setAttribute('class', 'preloader');

//             liName.innerHTML = `ID: ${episodeId}
//             Film name is: ${name}
//             <br>
//             Some info: ${openingCrawl}`;

//             const promises = characters.map(el => {

//                return fetch(el).then(response => response.json())
//             });

//             Promise.all(promises).then(data => {
//                const charactersList = data.map(({ name }) => {
//                   let liNames = document.createElement('ul');
//                   liNames.textContent = name;
//                   return liNames;
//                });

//                liName.append(...charactersList);
//                liName.removeChild(loader);
//             }).catch(error => {
//                console.log(error);
//             });

//             liName.append(loader);
//             return liName;
//          });

//          list.append(...filmName);
//          return list;
//       } catch (err) {
//          console.log(err);
//       }
//    }
// }
// const films = new Films(requestUrl);

// films.getFilms(requestUrl)
//    .then(response => {
//       document.body.append(films.renderName(response));
//    })
//    .catch((err) => {
//       console.log(err);
//    })

// class Films {
//    constructor(url) {
//       this.url = url;
//    }
//    getFilms(url) {

//       return fetch(url).then(response => {
//          return response.json()
//       }).catch((error) => {
//          alert(error);
//       })

// Асинхронность - это возможность получать ответ не сразу, а через время и затем его обрабатывать. Так как при обращении на сервер нам нужно ждать, пока придет информация.
// Тоесть, результат вернется не сразу, а при формировании до конча

// const ipLink = 'https://api.ipify.org/?format=json';
// const infoLink = 'http://ip-api.com/json/';
// const fields = 'fields=continent,country,regionName,city,district';
// const btn = document.querySelector('.find');
// class UserIpInfo {
//    async getIp(ipLink) {
//       const response = await fetch(ipLink);
//       return response.json();

//    }

//    async getInfo(ip) {
//       const response = await fetch(`${infoLink}${ip}?${fields}`);
//       return response.json();
//    }

//    async render(info) {
//       try {
//          const list = document.createElement('ul');
//          Object.keys(info).forEach(key => {
//             const item = document.createElement('li');
//             item.textContent = `${key.toUpperCase()}: ${info[key]}`;
//             list.append(item);
//          });
//          document.body.append(list);
//          return list;

//       } catch (err) {
//          console.log(err);
//       }
//    }

//    async handleClick() {
//       try {
//          const ipData = await this.getIp(ipLink);
//          const userIp = JSON.stringify(ipData).substring(7, 20);
//          const infoData = await this.getInfo(userIp);
//          await this.render(infoData);
//       } catch (error) {
//          console.error(error);
//       }
//    }
// }

// const user = new UserIpInfo();
// btn.addEventListener('click', () => user.handleClick());









